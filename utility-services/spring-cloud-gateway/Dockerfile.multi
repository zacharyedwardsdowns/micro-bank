# A global argument containing the working directory for the service.
ARG workingDirectory=/microbank/spring-cloud-gateway

# The gradle build stage of the service.
FROM gradle:7-jdk17 as build
ARG workingDirectory
WORKDIR ${workingDirectory}
# Cache dependencies
COPY ./build.gradle ./settings.gradle ./gradle.properties ./
RUN gradle -q assemble || return 0
# Gradle build
COPY . .
RUN gradle -q build

# Run the spring-cloud-gateway server.
FROM openjdk:17-oracle
ARG workingDirectory
WORKDIR ${workingDirectory}
COPY --from=build ${workingDirectory}/build/libs/*.jar ${workingDirectory}
CMD java -jar -Deureka.client.serviceurl.defaultzone=http://microbank-eureka:8761/eureka ./spring-cloud-gateway*.jar
